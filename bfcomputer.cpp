#include "bfcomputer.hpp"
#include <iostream>
#include <vector>

BFC::BFC(int size){
    memory = std::vector<char> (size, 0);
}

void BFC::incrementCell(int value){
    position += value;
}

void BFC::incrementValue(int value){
    memory.at(position) += value;
}

void BFC::outputByte(){
    std::cout << memory.at(position);
}

void BFC::inputByte(){
    std::cin >> memory.at(position);
}

char BFC::currentCellValue(){
    return memory.at(position);
}