This is a simple command line BrainFuck interpreter. It currently supports reading directly from text files, and outputting to stdout.

The project is setup to build with Bazel, to build, run `bazel build bfi`, and to run, `bazel run bfi`. 
You will need to change the argument to the program inside the BUILD file, to have it use your brainfuck script. 

*Manual compilation:*
  run `g++ main.cpp bfcomputer.cpp -o /path/to/executable`
  To run the executable, run `./bfi /path/to/brainfuck/file`

*Licence:* Do what you want with it, just link this repo with it.
