#include <vector>


class BFC{
    std::vector<char> memory;
    int position = 0;

public:
    BFC(int);
    void incrementCell(int);
    void incrementValue(int);
    char currentCellValue();
    void outputByte();
    void inputByte();

};
