#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <stack>
#include "bfcomputer.hpp"
#include "iso646.h"



bool isInvalid(char ch){
    //checks if character is an invalid brainfuck command
    return ch!='<' and ch!='>' and ch!='[' and ch!=']' and ch!='+' and ch!= '-' and ch!='.' and ch!=','; 
}

std::string readProgramFile(std::string filename){
    std::string program = "";
    std::ifstream pfile (filename);
    if(pfile.is_open()){
        std::string line;
        while (std::getline(pfile,line)){
            program += line;
        }
        pfile.close();
    }
    //cleanup of garbage characters
    program.erase(std::remove_if(program.begin(), program.end(), isInvalid), program.end());
    return program;
}

int main(int argc, char* argv[]){
    std::string program;
    if (argc == 2) {program = readProgramFile(argv[1]);
        if (program.length() == 0){
            std::cerr << "File is empty or does not exist." << std::endl;
            return 1;
        }

    }
    else{
        std::cerr << "Wrong number of arguments, run as" << std::endl << "./this_executable /path/to/file" << std::endl;
        return 1;
    }

    if(!program.empty()){
        BFC bfc (30000);
        const char* instruction = program.data();
        while(*instruction){
            switch(*instruction){
                case '>':
                    bfc.incrementCell(1);
                    break;
                case '<':
                    bfc.incrementCell(-1);
                    break;
                case '+':
                    bfc.incrementValue(1);
                    break;
                case '-':
                    bfc.incrementValue(-1);
                    break;
                case '.':
                    bfc.outputByte();
                    break;
                case ',':
                    bfc.inputByte();
                    break;
                case '[':
                    if(bfc.currentCellValue() == 0){
                        //skip to matching ]
                        int level = 0;
                        while(true){
                            instruction++;
                            if(*instruction == '['){
                                level++;
                            }
                            else if(level > 0 and *instruction == ']'){
                                level--;
                            }
                            else if(level == 0 and *instruction == ']'){
                                break;
                            }
                        }
                    }
                    break;
                case ']':
                    if(bfc.currentCellValue() != 0){
                        //skip back up to matching [
                        int level = 0;
                        while (true){
                            instruction--;
                            if(*instruction == ']'){
                                level++;
                            }
                            else if(level > 0 and *instruction == '['){
                                level--;
                            }
                            else if(level == 0 and *instruction == '['){
                                break;
                            }
                        }
                    }
                    break;

            }
            instruction++;
        }

    }

}